# Examples #

An overview of the example files in this folder.

To run them do:

    ./setlisp examples/<example-file>


### L-99.lisp ###

The 99 Lisp Problems tested against against setlisp, sbcl, and ecl. I only did the first ten, i'll get round to the rest at some point i swear...


### backquote.lisp ###

This wasn't added to _setlisp.rc_ because it wasn't fully implemented, it still works for the basic examples shown but not enough to be standard.


### builder-macro.lisp ###

My favourite part of SETL is the set builder notation so I just make system calls to a new setl instance for an inline use of it. 


### collatz.scl ###

A program that takes a set of numbers and applies the Collatz function to each member of the set until only the value 1 is left and returns the number of cycles it took to get there.


### fun_with_maps.scl ###

I'd recommend checking this file out, the implicit map type is an interesting feature of SETLisp and it is exampled in this file. (mapchoice) is also shown as a sibling of (mapcar).


### loops.lisp ###

An example of all the possible loop macro combinations supported (in/across/for/from/to/while/until/do/collect/collect-set/sum) and an example of all the possible do macro combinations.


### test.lisp ###

This was the original file I would update for basic testing / quick feedback, it has a variety of different things happening in it but nothing overly complicated. Notice that it uses strings instead of comments because i didn't add comments for quite a while.

