

(loop repeat 5 do
	(print "Loop!"))

(loop for ch across "Loop!" do
	(print ch))

(loop for n in (list 1 2 3 4) do
	(print n))

(loop for x from 0 below 5 do
	(print (loop for x from 0 to 5 collect x)))

(loop for x from 5 above 0 do
	(print (loop for x from 5 downto 0 collect x)))

(defvar foo 0)
(loop until (equal foo 5) do
	(progn
		(print foo)
		(setq foo (+ foo 1))))

(defvar bar 0)
(loop while (< bar 5) do
	(progn
		(print bar)
		(setq bar (+ bar 1))))

(loop for n from 0 to 100 sum n)











