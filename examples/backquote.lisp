
(defun eval-comma (v)
	(eval (make-symbol (subseq (symbol-name v) 1))))

(defun backquote-setlist-macro (obj type)
	(concatenate 'list
		(if (equal type 'LIST)
			(list (quote list))
			(list (quote setl)))
		(loop for val in obj collect
		(if (equal #\, (char (symbol-name val) 0))
			(list (quote quote) (eval-comma val))
			(list (quote quote) val)))))

(defun backquote-atom-macro (obj)
	(debug obj)
	(if (equal (type-of obj) 'SYMBOL)
		(if (equal #\, (char (symbol-name obj) 0))
			(eval-comma obj)
			(list (quote quote) obj))
		(list (quote quote) obj)))

(defun backquote-macro (s c)
	(let ((obj (read s)))
		(if (or (equal (type-of obj) 'LIST)
		        (equal (type-of obj) 'SET))
			(backquote-setlist-macro obj (type-of obj))
			(backquote-atom-macro obj))))
(set-macro-character #\` #'backquote-macro)


;; some basic examples of backquoting

(defvar bq 26)

(print `bq)
(print `,bq)
(print `(test toast ,bq))
(print `{test toast ,bq})
