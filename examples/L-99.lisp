
;;;
;;; Lisp 99 Problems (well... only ten of them).
;;; This is a print-friendly version of the
;;; test-ninty-nine.lisp in the integration tests.
;;;

(defun demo-macro (s c)
	(let ((obj (read s)))
		(format t "~a~%    ---> ~a~%~%" obj (eval obj))))
(set-macro-character #\! (function demo-macro))

;;; -------------------------------------------


;; P01: Find the last box of a list
(defun get-last (lst)
	(if (> (length lst) 1)
		(get-last (cdr lst))
		lst))
!(get-last (list 1 2 3 4))


;; P02: Find the last but one box of a list
(defun get-last-two (lst)
	(if (> (length lst) 2)
		(get-last-two (cdr lst))
		lst))
!(get-last-two (list 1 2 3 4))


;; P03: Find the K-th element of a list
(defun kth (k lst)
	(if (> k 1)
		(kth (- k 1) (cdr lst))
		(car lst)))
!(kth 3 (list (quote a) (quote b) (quote c) (quote d)))


;; P04: Find the number of elements of a list
(defun cardinality (lst)
	(if (null lst)
		0
		(+ 1 (cardinality (cdr lst)))))
!(cardinality (list 1 2 3 4 5))


;; P05: Reverse a list
(defun rev (lst)
	(rev2 lst (list)))

(defun rev2 (lst tmp)
	(if (null lst)
		tmp
		(rev2 (cdr lst) (cons (car lst) tmp))))
!(rev (list 1 2 3 4))


;; P06: Find out whether a list is a palindrome
(defun palinp (lst)
	(equal lst (rev lst)))
!(palinp (list 1 2 3 2 1))
!(palinp (list 1 2 3 4 5))


;; P07: Flatten a nested list structure
(defun flatten (lst)
	(rev (get-list lst (list))))

(defun get-list (lst tmp)
	(if (null (car lst))
		tmp
		(if (consp (car lst))
			(get-list (cdr lst) (append (get-list (car lst) (list)) tmp))
			(get-list (cdr lst) (cons (car lst) tmp)))))

!(flatten (list 1 (list 2 (list 3 4) 5)))


;; P08: Eliminate consecutive duplicates of list elements
(defun remove-dup (lst)
	(rm-dup lst (list)))

(defun rm-dup (lst tmp)
	(if (null (car lst))
		(rev tmp)
		(if (equal (car lst) (car tmp))
			(rm-dup (cdr lst) tmp)
			(rm-dup (cdr lst) (cons (car lst) tmp)))))

!(remove-dup (list 1 1 1 2 3 3 3 4 4 1 1 1 1 1 1 1 1 1 2))


;; P09: Pack consecutive duplicates of list elements into sublists
(defun pack (input)
        (pack-lst nil (list (car input)) (cdr input)))

(defun pack-lst (output packet input)
	(if (null input)
		(rev (cons packet output))
		(if (equal (car input) (car packet))
			(pack-lst output (cons (car input) packet) (cdr input))
			(pack-lst (cons packet output) (list (car input)) (cdr input)))))

!(pack '(a a a a b c c a a d e e e e))


;; P10: Run-length encoding of a list
(defun run-encode (input)
	(count-packets nil (pack input)))

(defun count-packets (output input)
	(if (null input)
		(rev output)
		(count-packets (cons (list (length (car input)) (car (car input))) output) (cdr input))))

!(run-encode '(a a a a b c c a a d e e e e))
