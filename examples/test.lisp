
"Defining and changing a variable value"
(defvar var 5)
(print var)
(set (quote var) 6)
(print var)
(setq var 7)
(print var)

"Arithmetic operations"
(print (+ var 99))
(print (/ var var 2 4))

"Control flow"
(if (> 5 4)
	(print (quote yes))
	(print (quote no)))

"Defining a function (recursive)"
(defun fact (n)
	(if (<= n 1)
		1
		(* n (fact (- n 1)))))
"NOTE: Segfault on low memory virtual machine. Worked"
"      fine after Increasing RAM on VM from 4gb to 8gb."
"(print (fact 50))"


"Defining a function (multi-expression body)"
(defun things (a b c)
	(print a)
	(print b)
	(print c))
(things 1 2 3)

"Load in a function"
(load "examples/support_files/factorial.lisp")
(print (factorial 5))

"Special printing methods"
(print "Testing \"testing 1 2\" 3")
(princ "Testing \"testing 1 2\" 3") (terpri)
(format t "Testing \"testing 1 2\" 3 ~a~%" "(via \"format\")")

"Function/funcall"
(print (function abs))
(print (function fact))
(print (funcall (function fact) 5))

"Mapcar"
(print (mapcar (function fact) (list 1 2 3 4)))

"Type tests"
(print (list 1 2 9))
(print (type-of (list 1 2 9)))
(print (type-of (function things)))

"Lets"
(defvar varname "Cats")
(princ varname) (terpri)
(let ((varname 666))
	(princ varname))
(princ varname) (terpri)

(let ((a 1) (b 2) (c 3))
	(let ((b 4) (c 5))
		(let ((c 6))
			(print (list a b c)))
		(print (list a b c)))
	(print (list a b c)))

"------- SETL extension -------"

"Sets"
(defvar *my-set* (setl -1 2 -3 4 -5))
(print *my-set*)
(setq *my-set* (+ *my-set* (setl 6 6 6)))
(print *my-set*)
(print *my-set*)

"SET-expressions"
{print {+ 1 2 3 3 2 1}}
{print {list 12 11 -10 9 8 -7 6 5 4 2 -3 1 0 0 0}}

"SETL types"
(print (setl (list "my" (quote map))))
(print (type-of (setl (list (quote my) "map"))))
(print (setl 1 2 9))
(print (type-of (setl 1 2 9)))
