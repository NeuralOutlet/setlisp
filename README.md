# SETLisp #

Inspired by Peter Norvig's [Scheme interpreter in Python](http://norvig.com/lispy.html), but alas not in 90 lines, SETLisp is an interpreter for Common Lisp written in [SETL](http://setl.org/doc/setl.html). It is very small in terms of what it can do, but also adds some SETL functions and functionality - a superset of a subset if you will. The reason for doing this was a general learning project for me in both SETL and Common Lisp but also to experiment with adding Set Theory into Lisp.

### Introducing Sets to Lisp ###

S-expressions are famously lists, in SETLisp there are also SET-expressions which are, wait for it, sets. The two differing factors is that sets do not allow duplicates and order doesn't matter. An example of the former can be seen in using the addition function where duplicates aren't added:

![set-expressions.png](https://bitbucket.org/repo/gpoEob/images/1907980834-set-expressions.png)

You can see from the above image that sets are denoted by '{' and '}'. The sister function to (list 1 2 3) is (setl 1 2 3), and their cousins are {list 1 2 3} and {setl 1 2 3}. The first attribute of sets is easy to implement, especially with a SETL back-end, there simply aren't duplicates in sets - but the second attribute non-specific order does kind of ruin the expression's notation. Because of this a SET-expression is a function over a set which gives them somewhat of a blackbox property. Notice that having two types of expressions opens up the language to overriding function names. {defun} is a special case where order does matter, it is overriden to cope with both s-exp and set-exp definitions:

    (defun name (params...) body) ; params lines up with vars passed to the function
    {defun name params body}      ; params accepts variadic vars passed to the function
    
    (defun examp (a b c) (+ a b c))
    (examp 1 2 3) ;valid
    (examp 1 2)   ;invalid
    
    {defun examp nums (loop for v in nums sum v)}
    {defun examp nums (reduce #'+ nums)}  ;overwrite with alt version
    {examp 1 2 3} ;valid
    {examp 1 2}   ;also valid
    {examp}       ;still valid
    
    ;; Note: these are the same because of duplicates
    {examp 1 1 1 2 1 2 2 2 2}
    {examp 1 2}
    

An entire list of functions (s-exp and set-exp) can be found in the [doc](https://bitbucket.org/NeuralOutlet/setlisp/src/master/doc/) folder which has a Language reference and Library reference.

### Setup & Running ###

You need to get the GNU SETL compiler and the source code from this repo, to do that do the following in your terminal:

    # 32bit
    wget http://setl.org/setl/bin/Linux-x86-32bit/setlbin.tgz
    # 64bit
    wget http://setl.org/setl/bin/Linux-x86-64bit/setlbin.tgz
    
    sudo tar -zxvf setlbin.tgz -C /usr/bin/
    git clone https://bitbucket.org/NeuralOutlet/setlisp
    cd setlisp

You can open up a REPL by simply calling the interpreter:

    ./setlisp

Or you can read a script file *.lisp like so:

    ./setlisp myfile.lisp # reads lisp code

Some functionality such as 'quote, #'function, #\Character, and ; comments are defined in the setlisp.rc file. this is automatically loaded in before anything unless the file doesn't exist in the local folder or $HOME.

### Examples ###

In the examples folder there are some lisp (\*.lisp) and setlisp (\*.scl) files showing some of the capability of this implementation. You can write purely Common Lisp functions and your script will be able to be run with setlisp or a normal CL interpreter (I tested against SBCL & ECL). Below you can see a snippit of _[examples/test.lisp](https://bitbucket.org/NeuralOutlet/setlisp/src/master/examples/test.lisp)_ (an early file I used for quick feedback):


![screenshot.png](https://bitbucket.org/repo/gpoEob/images/3362764975-screenshot.png)