$ This file contains the definitions for string, list, and set functions.

$( setl values...
$? Create a set from the set of values
proc scl_setl(sargs, env);
	if is_nil sargs then;
		return [false, env];
	end if;

	out := {}; -- a set
	for i in {1..#sargs} loop;
		out +:= {sargs(i)};
	end loop;
	return [out, env];
end proc;

$( list values...
$? Returns a list of the values
proc scl_list(sargs, env);
	if is_nil sargs then;
		return [false, env];
	end if;

	out := []; -- a sequence (list)
	omcount := 0;
	nil := om;
	for i in {1..#sargs} loop;
		if omcount > 0 then;
			out +:= [[1, om, 1] : j in {1..omcount}] + [sargs(i)];
			omcount := 0;
		else;
			if sargs(i) = om then;
				omcount +:= 1;
			end if;
			out +:= [sargs(i)];
		end if;
	end loop;

	for k in {1..#out} loop;
		if out(k) = [1, om, 1] then;
			out(k) := out(k)(2);
		end if;
	end loop;

	return [out, env];
end proc;

$( length set-list-or-string
$? Returns length of a set, list, or string.
proc scl_length(sargs, env);
	myseq := sargs(1);

	-- internal type container
	if is_tuple myseq and is_routine myseq(1) then;
		return [#myseq - 1, env];
	end if;

	-- string
	if is_string myseq and myseq(1) = '"' then;
		return [#unpretty(myseq), env];
	end if;

	return [#myseq, env];
end proc;

$( car some-list
$? Returns the car of 'some-list'.
proc scl_car(sargs, env);
	if is_tuple sargs(1) then;
		output := sargs(1)(1);
		return [output ? false, env];
	end if;
	if sargs(1) = false then;
		return [false, env];  $ (car nil) -> nil
	end if;

	param_type_error("car", 1, "cons", type(sargs(1)));
end proc;

$( cdr some-list
$? Returns the cdr of 'some-list'.
proc scl_cdr(sargs, env);
	out := sargs(1);
	waste fromb out;
	return [out, env];
end proc;

$( cons value some-list
$? Creates a cons cell with 'value' as the car and 'some-list' as the cdr.
proc scl_cons(sargs, env);
	if is_boolean sargs(2) and not sargs(2) then;
		return [[sargs(1)], env];
	end if;
	out := [sargs(1)] + sargs(2);
	return [out, env];
end proc;

$( subseq seq start end
$? Returns the subsequence of 'str' from position 'start' to position 'end'.
$? If no 'end' is given then the end of 'seq' is used.
$? NOTE: The SETLisp version of this function is more liberal and accepts lists aswell as strings.
proc scl_subseq(sargs, env);
	myseq := sargs(1);
	if is_string myseq and myseq(1) = '"' then;
		strseq  := myseq(2..#myseq-1);
		seqs := sargs(2);
		seqe := sargs(3) ? #strseq;
		return ['"'+strseq(seqs+1..seqe)+'"', env];
	end if;

	if is_tuple myseq then;
		seqs := sargs(2);
		seqe := sargs(3) ? #myseq;
		return [myseq(seqs+1..seqe), env];
	end if;
	return [om, env];
end proc;

$[ from some-set
$? Returns a nondeterministically chosen element from 'some-set'. Not sure the underworkings of SETL's 'from' function but seems to just take from the "front" of the set. See also 'arb' and 'random-element'.
proc setl_from(sargs, env);
	myobj from sargs;
	[myset, env] := ast_eval(myobj, env);
	if is_nil myset then;
		return [false, env];
	end if;
	output from myset;
	if is_string myobj then;
		env(myobj) := myset;
	end if;
	return [output, env];
end proc;
proc scl_from(sargs, env);
	myobj := sargs(1);
	[myset, env] := ast_eval(myobj, env);
	if is_nil myset then;
		return [false, env];
	end if;
	output from myset;
	if is_string myobj then;
		env(myobj) := myset;
	end if;
	return [output, env];
end proc;

$[ random [int-val/real-val/sequence-val]
$? For 'int-val' random returns a random number between 0 and 'int-val'.\nFor 'real-val' random returns a real number within the interval between 'real-val' and 0.\nFor 'sequence-val' random returns a randomly chosen element from the list/set/string 'some-sequence'. See also 'arb' and 'from'.
proc setl_random(sargs, env);
	myobj from sargs;
	if is_nil myobj then;
		return [false, env];
	end if;
	if is_string myobj then;
		if myobj(1) /= '"' then;
			printa(stderr, "'random' does not take a symbol as argument.");
			return [om, env];
		end if;
		myobj := myobj(2..#myobj-1);
	end if;
	return [random(myobj), env];
end proc;
proc scl_random(sargs, env);
	myobj := sargs(1);
	if is_nil myobj then;
		return [false, env];
	end if;
	if is_string myobj then;
		if myobj(1) /= '"' then;
			printa(stderr, "'random' does not take a symbol as argument.");
			return [om, env];
		end if;
		myobj := myobj(2..#myobj-1);
	end if;
	return [random(myobj), env];
end proc;

$[ arb some-set
$? Returns a non-deterministically chosen element from 'some-set'. Unlike the function 'from' this does not have a side-effect on the set itself. See also 'from' and 'random'.
proc setl_arb(sargs, env);
	myobj from sargs;
	if is_nil myobj then;
		return [false, env];
	end if;
	if not is_set myobj then;
		printa(stderr, "'arb' takes a set as argument.");
		return [om, env];
	end if;
	return [arb(myobj), env];
end proc;
proc scl_arb(sargs, env);
	myobj := sargs(1);
	if is_nil myobj then;
		return [false, env];
	end if;
	if not is_set myobj then;
		printa(stderr, "'arb' takes a set as argument.");
		return [om, env];
	end if;
	return [arb(myobj), env];
end proc;

${ mapchoice some-function some-set
$? Applies 'some-function' to all members of 'some-set'. Chosen in a nondeterministic order.
$? Internally uses the SETL function 'from'. Type of params are checked to decide what to use.
proc setl_mapchoice(sargs, env);
	myfunc := om;
	myset := om;
	if #sargs = 2 then;
		for item in sargs loop;
			if is_map item and item(routine func_id) /= om then;
				myfunc := item;
			end if;
			if is_set item then;
				myset := item;
			end if;
		end loop;
		if myfunc = om then;
		end if;
		if myset = om then;
		end if;

		out := {};
		for i in {1..#myset} loop;
			next_choice from myset;
			[result, env] := scl_funcall([myfunc, next_choice], env);
			out +:= {result};
		end loop;
		return [out, env];
	else;
		return [om, env];
	end if;
end proc;

$( mapchoice some-function some-set
$? Applies 'some-function' to all members of 'some-set'. Chosen in a nondeterministic order.
$? Internally uses the SETL function 'from'. Type of params are checked to decide what to use.
proc scl_mapchoice(sargs, env);
	if #sargs = 2 then;
		out := [];
		mfunc := sargs(1);
		mset := sargs(2);
		for i in {1..#mset} loop;
			next_choice from mset;
			[result, env] := scl_funcall([mfunc, next_choice], env);
			out +:= [result];
		end loop;
		return [out, env];
	else;
		return [om, env];
	end if;
end proc;

${ list values...
$? Creates a list from the set of values. This means duplicates
$? and order are ignored as if (setl values...) was called but
$? returned a list instead of a set.
-- {list c b a} has a slight difference
-- to (list c b a) in that the former
-- might creat a list [a b c] instead.
proc setl_list(sargs, env);
	setargs := {a : a in sargs};
	if is_nil setargs then;
		return [false, env];
	end if;
	out := []; -- a sequence (list)
	for i in {1..#setargs} loop;
		item from setargs;
		out +:= [item];
	end loop;
	return [out, env];
end proc;

${ setl values...
$? Create a set from the set of values
proc setl_setl(sargs, env);
	if is_nil sargs then;
		return [false, env];
	end if;
	return [sargs, env];
end proc;

$( mapcar function-type some-list
$? Applies 'function-type' to all members of 'some-list'.
proc scl_mapcar(sargs, env);
	out := [];
	mfunc := sargs(1);
	mlist := sargs(2);
	for i in {1..#mlist} loop;
		out +:= [scl_funcall([mfunc, mlist(i)], env)(1)];
	end loop;
	return [out, env];
end proc;

$( char some-string index
$? Returns the character at 'index' in 'some-string'.
proc scl_char(sargs, env);
	if is_string sargs(1) and sargs(1)(1) = '"' then;
		mystr := sargs(1)(2..#sargs(1)-1);
		return ['"'+mystr(sargs(2)+1)+'"', env];
	end if;
	return [om, env];
end proc;

$( concatenate type-id sequences...
$? Joins sequences of the same type, type-id is 'string 'list or 'setl.
proc scl_concatenate(sargs, env);
	if sargs(1) = 'STRING' then;
		output := "";
		for sequence in sargs(2..#sargs) loop;
			output +:= sequence(2..#sequence-1);
		end loop;
		return ['"'+output+'"', env];
	end if;

	if sargs(1) = 'LIST' then;
		output := [];
		for sequence in sargs(2..#sargs) loop;
			output +:= sequence;
		end loop;
		return [output, env];
	end if;

	if sargs(1) = 'SETL' then;
		output := {};
		for sequence in sargs(2..#sargs) loop;
			output +:= sequence;
		end loop;
		return [output, env];
	end if;

	return [om, env];
end proc;
