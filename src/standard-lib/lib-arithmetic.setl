
$( + values...
$? The plus operator - used for addition, but inherently
$? works for concatenation of strings or lists and set union.
proc scl_add(sargs, env);
	argtypes := {type(a) : a in sargs};
	if #argtypes > 1 then;
		misc_error("Error: '+' requires all argument types to be the same." +
		           "\n       User input:\n       "+ sexp_type_example("+", sargs));
	end if;
	return [+/sargs, env];
end proc;

$( - values...
$? The minus operator - used for subtraction, but inherently
$? works for set difference
proc scl_minus(sargs, env);
	argtypes := {type(a) : a in sargs};
	if #argtypes > 1 then;
		misc_error("Error: '-' requires all argument types to be the same." +
		           "\n       User input:\n       "+ sexp_type_example("-", sargs));
	end if;
	return [-/sargs, env];
end proc;

$( \* values...
$? The product operator - used for multiplication, but inherently
$? works for set intersection and string/list replication.
proc scl_times(sargs, env);
	is_str_multi := false;
	for i in {1..#sargs} loop;
		param := sargs(i);
		if is_string param and param(1) = '"' then;
			sargs(i) := param(2..#param-1);
			is_str_multi := true;
		end if;
	end loop;

	if is_str_multi then;
		return ['"' + */sargs + '"', env];
	end if;
	return [*/sargs, env];
end proc;

$( / values...
$? The division operator - used for division
proc scl_div(sargs, env);
	return [//sargs, env];
end proc;

$( div valA valB
$? The integer division operator - used for division but will only return integer values. A normal Common Lisp approach would be to use 'truncate'. 'div' is a SETL function.
proc scl_div_int(sargs, env);
	if #sargs /= 2 then;
		params_count_error("div", 2, #sargs);
	end if;
	return [sargs(1) div sargs(2), env];
end proc;

$( max values...
$? Returns maximum value from values
proc scl_max(sargs, env);
	for sarg in sargs loop;
		if not is_numeric sarg then;
			misc_error("Error: 'max' requires all arguments be numeric." +
			           "\n       User input:\n       "+ sexp_type_example("max", sargs));
		end if;
	end loop;
	return [max/sargs, env];
end proc;

$( min values...
$? Returns the minimum value from values
proc scl_min(sargs, env);
	for sarg in sargs loop;
		if not is_numeric sarg then;
			misc_error("Error: 'min' requires all arguments be numeric." +
			           "\n       User input:\n       "+ sexp_type_example("min", sargs));
		end if;
	end loop;
	return [min/sargs, env];
end proc;

${ + values...
$? The plus operator - used for addition, but inherently
$? works for concatenation of strings or lists and set union.
proc setl_plus(sargs, env);
	argtypes := {type(a) : a in sargs};
	if #argtypes > 1 then;
		misc_error("Error: '+' requires all argument types to be the same");
	end if;
	return [+/sargs, env];
end proc;

${ - values...
$? The minus operator - used for subtraction, but inherently
$? works for set difference
proc setl_minus(sargs, env);
	argtypes := {type(a) : a in sargs};
	if #argtypes > 1 then;
		misc_error("Error: '-' requires all argument types to be the same");
	end if;
	return [-/sargs, env];
end proc;

${ \* values...
$? The product operator - used for multiplication, but inherently
$? works for set intersection and string/list replication.
proc setl_times(sargs, env);
	return [*/sargs, env];
end proc;

${ / values...
$? The division operator - used for division
proc setl_div(sargs, env);
	return [//sargs, env];
end proc;

${ min values...
$? Returns the minimum value from values
proc setl_min(sargs, env);
	return [min/sargs, env];
end proc;

${ max values...
$? Returns maximum value from values
proc setl_max(sargs, env);
	return [max/sargs, env];
end proc;

$[ abs value
$? Returns the absolute value of 'value'.
proc scl_abs(sargs, env);
	if #sargs = 1 then;
		return [abs(sargs(1)), env];
	else;
		params_count_error('abs', 1, #sargs);
	end if;
end proc;

$( log value
$? Natural logarithm, returns nil where 'value' equals 0.
proc scl_log(sargs, env);
	if #sargs = 1 then;
		if is_numeric sargs(1) then;
			if sargs(1) > 0 then;
				return [log(sargs(1)), env];
			else;
				return [false, env];
			end if;
		else;
			param_type_error('log', 1, 'number', type(sargs(1)));
		end if;
	else;
		params_count_error('log', 1, #sargs);
	end if;
	return [om, env];
end proc;

$( exp value
$? Natural exponential, e to the power 'value'.
proc scl_exp(sargs, env);
	if #sargs = 1 then;
		if is_numeric sargs(1) then;
			return [exp(sargs(1)), env];
		else;
			param_type_error('exp', 1, 'number', type(sargs(1)));
		end if;
	else;
		params_count_error('exp', 1, #sargs);
	end if;
	return [om, env];
end proc;

$( expt base power
$? Returns 'base' raised to the power of 'power'.
proc scl_expt(sargs, env);
	if #sargs = 2 then;
		if is_numeric sargs(1) and is_numeric sargs(2) then;
			return [sargs(1) ** sargs(2), env];
		else;
			if not is_numeric sargs(1) then;
				param_type_error('expt', 1, 'number', type(sargs(1)));
			else;
				param_type_error('expt', 2, 'number', type(sargs(2)));
			end if;
		end if;
	else;
		params_count_error('expt', 2, #sargs);
	end if;
	return [om, env];
end proc;
