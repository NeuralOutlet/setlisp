# Language Reference #

Fingers crossed this Common Lisp interpreter has the same syntax as Common Lisp with the exception of the SETL Extensions.

### Uncommon Lisp (SETL Extensions) ###

Set-expressions & overloading: With the introduction of set-expressions as explicitly different from s-expressions we open up the idea of overloading. (foo 1 2 3) can act differently to {foo 1 2 3}, the definition of s-expressions are the same - set-expressions are defined with the form {defun func-name params body...}. This is the only set-expression to actively care about order of parameters (under the hood it is a list, not a set). The big difference is 'params', it's a blackbox and contains all the args passed to the function. After that 'body' is an implicit progn just like in (defun). parameters are taken from the set using from or other set accessors. We can do for example:

    (defun foo (a b c) (+ a b c (* a b c)))

    {defun foo vals (reduce #'+ (+ vals (reduce #'* vals)))}

which would act the same if given unique numbers as arguments. But as the {foo} function takes its params to be a set it will remove duplicates.

Liberal Common Lisp: Many of the functions with Common Lisp names act _like_ CL but also extend beyond that. This can be seen above with the '+' operator, it is numeric addition (or affermation), and also string/list/set concatenation. Another example is 'subseq' which can give lists aswell as strings.

'eval-setl' & 'read-setl': These two functions are for dealing with setl code, eval-setl calls a subprocess of another instance of setl via system(). The 'read-setl' command is very gentle, it reads SETL values and returns them as Lisp values (though they are very similar anyway). Together they help make the builder-macro which allows inline SETL list/set comprehension like ${ 2*x | x in {0..20} | x mod 4 > 2} which would yield {6 14 22 30 38}.

### Comments, Quotes, and other Macros ###

The SETLisp interpreter is quite minimal in functionaty, but to make it feel more conformant with traditional Common Lisp I read macros. In the setlisp.rc file I define comments, characters, quotes, and hashquotes. I did not add defmacro, sorry :(. Of those macros characters are the most different, because the underlying type is not prevelant in SETL. It really is just for show. 

As mentioned above there is also the setl builder-macro for sets and lists. It is a dirty dirty thing that I love. Don't kinkshame me.

Read macros added in setlisp.rc:

* Characters \#\\\!
* ;comments
* 'quotes
* \#'functions
* \#+setlisp (print "Feature Expressions (does nothing other than this)")
* \#| multi-line comments |\#

In the examples folder there are also definitions for `(back ,quotes) and SETL set builder notation.



# Library Reference #

The following documentation on available functions is generated by update.setl in this folder.


**Available s-expressions:**       +, -, /, <, <=, =, >, >=, \*, car, cdr, char, char-code, code-char, concatenate, cons, defvar, div, do, domain, eval, eval-setl, exit, exp, expt, format, funcall, function, getenv, in, length, list, log, loop, make-dispatch-macro-character, make-symbol, mapcar, mapchoice, mapp, max, min, mref, null, oddp, peek-char, quit, range, read, read-char, read-setl, set, set-dispatch-macro-character, set-macro-character, setl, subseq, symbol-name, terpri, values, write.




**Available set-expressions:**     +, -, /, \*, exit, list, mapchoice, max, min, quit, setl.




**Expressions available as both:** abs, and, arb, booleanp, consp, directory, from, get-args, integerp, listp, load, not, numberp, or, princ, print, random, read-line, realp, setlp, stringp.





- - - - -

## + ##

*(+ values...)*

The plus operator - used for addition, but inherently works for concatenation of strings or lists and set union.


- - - - -

## - ##

*(- values...)*

The minus operator - used for subtraction, but inherently works for set difference


- - - - -

## \* ##

*(\* values...)*

The product operator - used for multiplication, but inherently works for set intersection and string/list replication.


- - - - -

## / ##

*(/ values...)*

The division operator - used for division


- - - - -

## div ##

*(div valA valB)*

The integer division operator - used for division but will only return integer values. A normal Common Lisp approach would be to use 'truncate'. 'div' is a SETL function.


- - - - -

## max ##

*(max values...)*

Returns maximum value from values


- - - - -

## min ##

*(min values...)*

Returns the minimum value from values


- - - - -

## + ##

*{+ values...}*

The plus operator - used for addition, but inherently works for concatenation of strings or lists and set union.


- - - - -

## - ##

*{- values...}*

The minus operator - used for subtraction, but inherently works for set difference


- - - - -

## \* ##

*{\* values...}*

The product operator - used for multiplication, but inherently works for set intersection and string/list replication.


- - - - -

## / ##

*{/ values...}*

The division operator - used for division


- - - - -

## min ##

*{min values...}*

Returns the minimum value from values


- - - - -

## max ##

*{max values...}*

Returns maximum value from values


- - - - -

## abs ##

*(abs value)*

*{abs value}*

Returns the absolute value of 'value'.


- - - - -

## log ##

*(log value)*

Natural logarithm, returns nil where 'value' equals 0.


- - - - -

## exp ##

*(exp value)*

Natural exponential, e to the power 'value'.


- - - - -

## expt ##

*(expt base power)*

Returns 'base' raised to the power of 'power'.


- - - - -

## null ##

*(null value)*

Checks if 'value' is nil (includes false, empty set/list, and Om)


- - - - -

## not ##

*(not value)*

*{not value}*

Negates 'value'.


- - - - -

## and ##

*(and conditions...)*

*{and conditions...}*

Returns the last condition evaluated if all of the conditions are non-nil (true), else returns nil.


- - - - -

## or ##

*(or conditions...)*

*{or conditions...}*

Returns the first evaluated condition that it non-nil (true), else returns nil otherwise.


- - - - -

## = ##

*(= var1 var2)*

Returns true if var1 is equal to var2.


- - - - -

## > ##

*(> var1 var2)*

Returns true if var1 is greater than var2.


- - - - -

## < ##

*(< var1 var2)*

Returns true if var1 is less than var2.


- - - - -

## >= ##

*(>= var1 var2)*

Returns true if var1 is greater than or equal to var2.


- - - - -

## <= ##

*(<= var1 var2)*

Returns true if var1 is less than or equal to var2.


- - - - -

## oddp ##

*(oddp val)*

Checks if value is odd, returns true if it is, nil otherwise.


- - - - -

## in ##

*(in value some-set)*

Membership test for the value in some-set


- - - - -

## function ##

*(function function-symbol)*

Returns a function type for the given 'function-symbol'.


- - - - -

## funcall ##

*(funcall function-type values...)*

Calls function 'function-type' with the given 'values'.


- - - - -

## do ##

*(do ((count-name init-val increment-expression)...) (test-form result-form))*

Does increment-form until test-form is met then returns result-form
 Example: (do ((i 0 (+ i 1))) ((= 10 i) (code-char i))) ; returns a line-feed after counting to 10.


- - - - -

## loop ##

*(loop ...)*

This is an attempt to match some of the more often used Common Lisp loops, which are:

    loop for 'var' [in/across] 'sequence' [do/collect/set-collect] 'form'
    loop for 'var' [in/across] 'sequence' [while/until] 'condition' [do/collect/set-collect] 'form'
    loop for 'var' from 'val' [to/downto/above/below] 'val2' [do/collect/set-collect] 'form'
    loop for 'var' from 'val' [to/downto/above/below] 'val2' [while/until] 'condition' [do/collect/set-collect] 'form'
    loop [until/while] 'condition' [do/collect/set-collect] 'form'
    loop repeat 'int-value' [do/collect/set-collect] 'form'

 Repeats an expression given a specific condition. Not very Common Lisp conformant.


- - - - -

## mref ##

*(mref map key [value])*

Losely styled on Common Lisp's 'aref', 'mref' gets or sets an element in a map. Maps are an implicit type in SETLisp - they are a set of 2-tuples like {("first-key" 5) ("second-key" 22)}.
    (mref map key) will return the value of 'key' in 'map' and (mref map key value) will set 'key' in 'map' as 'value'.


- - - - -

## domain ##

*(domain some-map)*

Returns a set of all key names in the map.


- - - - -

## range ##

*(range some-map)*

Returns a set of all values in the map.


- - - - -

## exit ##

*{exit}*

Leave the REPL


- - - - -

## quit ##

*{quit}*

Leave the REPL


- - - - -

## quit ##

*(quit)*

Leave the REPL.


- - - - -

## exit ##

*(exit)*

Leave the REPL.


- - - - -

## eval-setl ##

*(eval-setl type input)*

Evaluates SETL code by calling SETL's 'system' function (probably similar or a wrapper for C's system function) and running a subprocess of SETL. Takes a quote 'type' which is either 'string, 'stream or 'script. If 'string then 'input' is called by SETL in the commandline mode: $ setl 'print("SETL code");' If 'script is passed then the script is called: $ setl myscript.setl.


- - - - -

## code-char ##

*(code-char val...)*

takes byte int value 'val' and returns ASCII character for it. If more than one val is given then a list of results is returned.


- - - - -

## char-code ##

*(char-code val...)*

Takes character 'val' and returns integer value for it. If more than one val is given then a list of results is returned.


- - - - -

## set-macro-character ##

*(set-macro-character macro-char function-type)*

Sets the macro for 'macro-char' which will be called when reading the code and 'function-type' we be executed on the stream. 'function-type' should have been befined in the form: (defun blah (stream char) ...)


- - - - -

## set-dispatch-macro-character ##

*(set-dispatch-macro-character dispatch-char sub-char function-type)*

Sets the dispatch macro for those characters which will be called when reading the code and 'function-type' we be executed on the stream. 'function-type' should have been befined in the form: (defun blah (stream char num) ...)


- - - - -

## make-dispatch-macro-character ##

*(make-dispatch-macro-character dispatch-char)*

does nothing at the moment because there is no readtable - everything is fair game


- - - - -

## values ##

*(values)*

Returns nothing (Om not nil)


- - - - -

## eval ##

*(eval 'lisp-object')*

Evaluates 'lisp-object', for example (eval '(+ 5 5 5)) returns 15.


- - - - -

## format ##

*(format stream fmt-string values...)*

Writes 'fmt-string' to 'stream' using escape characters and 'values'. Supports ~a and ~% escape characters only.


- - - - -

## print ##

*(print value)*

*{print value}*

Shorthand for (write t value)


- - - - -

## princ ##

*(princ value)*

*{princ value}*

Shorthand for (write nil value)


- - - - -

## terpri ##

*(terpri output-stream)*

Outputs new line to the 'output-stream'. If no argument is given it is sent to *standard-output*.


- - - - -

## write ##

*(write raw value output-stream)*

If 'raw' is true then 'value' is printed to 'output-stream' without being evaluated (\" et al are kept as is). If no 'stream-output' is given the default is *standard-output*.


- - - - -

## read-char ##

*(read-char stream-name)*

Reads the next character from stream 'stream-name'.


- - - - -

## peek-char ##

*(peek-char stream-name)*

Reads the next character from stream 'stream-name' without taking it off the stream.


- - - - -

## read-line ##

*(read-line stream-name)*

*{read-line stream-name}*

Returns a string of the next line in stream 'stream-name'.


- - - - -

## read ##

*(read stream-name eof? eofchar recursive?)*

Reads the next lisp object from stream 'stream-name'.


- - - - -

## read-setl ##

*(read-setl type input)*

'type' is either 'string or 'stream. 'read-setl' will read 'input' as a SETL value or set builder such as "[x*2 | x in {0..5}]" which evaluates to the Lisp (0 2 4 6 8 10). The return value is the Lispified SETL value. Ideally when 'string is used a second value of string position would be returned - but this is not the case.


- - - - -

## load ##

*(load filename)*

*{load filename}*

Loads in Lisp code from the filename.


- - - - -

## setl ##

*(setl values...)*

Create a set from the set of values


- - - - -

## list ##

*(list values...)*

Returns a list of the values


- - - - -

## length ##

*(length set-list-or-string)*

Returns length of a set, list, or string.


- - - - -

## car ##

*(car some-list)*

Returns the car of 'some-list'.


- - - - -

## cdr ##

*(cdr some-list)*

Returns the cdr of 'some-list'.


- - - - -

## cons ##

*(cons value some-list)*

Creates a cons cell with 'value' as the car and 'some-list' as the cdr.


- - - - -

## subseq ##

*(subseq seq start end)*

Returns the subsequence of 'str' from position 'start' to position 'end'. If no 'end' is given then the end of 'seq' is used. NOTE: The SETLisp version of this function is more liberal and accepts lists aswell as strings.


- - - - -

## from ##

*(from some-set)*

*{from some-set}*

Returns a nondeterministically chosen element from 'some-set'. Not sure the underworkings of SETL's 'from' function but seems to just take from the "front" of the set. See also 'arb' and 'random-element'.


- - - - -

## random ##

*(random [int-val/real-val/sequence-val])*

*{random [int-val/real-val/sequence-val]}*

For 'int-val' random returns a random number between 0 and 'int-val'.
For 'real-val' random returns a real number within the interval between 'real-val' and 0.
For 'sequence-val' random returns a randomly chosen element from the list/set/string 'some-sequence'. See also 'arb' and 'from'.


- - - - -

## arb ##

*(arb some-set)*

*{arb some-set}*

Returns a non-deterministically chosen element from 'some-set'. Unlike the function 'from' this does not have a side-effect on the set itself. See also 'from' and 'random'.


- - - - -

## mapchoice ##

*{mapchoice some-function some-set}*

Applies 'some-function' to all members of 'some-set'. Chosen in a nondeterministic order. Internally uses the SETL function 'from'. Type of params are checked to decide what to use.


- - - - -

## mapchoice ##

*(mapchoice some-function some-set)*

Applies 'some-function' to all members of 'some-set'. Chosen in a nondeterministic order. Internally uses the SETL function 'from'. Type of params are checked to decide what to use.


- - - - -

## list ##

*{list values...}*

Creates a list from the set of values. This means duplicates and order are ignored as if (setl values...) was called but returned a list instead of a set.


- - - - -

## setl ##

*{setl values...}*

Create a set from the set of values


- - - - -

## mapcar ##

*(mapcar function-type some-list)*

Applies 'function-type' to all members of 'some-list'.


- - - - -

## char ##

*(char some-string index)*

Returns the character at 'index' in 'some-string'.


- - - - -

## concatenate ##

*(concatenate type-id sequences...)*

Joins sequences of the same type, type-id is 'string 'list or 'setl.


- - - - -

## directory ##

*(directory pattern)*

*{directory pattern}*

A Lisp front for SETL's _glob_ function. Takes a path 'pattern' like "*.h" and searches the current dir, returning a list of results.


- - - - -

## get-args ##

*(get-args detect-type)*

*{get-args detect-type}*

Returns the command line args as a list of either strings or atoms.


- - - - -

## getenv ##

*(getenv var-name)*

If 'var-name' exists as a system enviroment variable then it returns the value. 'var-name' can be a string or a symbol.


- - - - -

## numberp ##

*(numberp value)*

*{numberp value}*

Returns true if 'value' is any number type.


- - - - -

## realp ##

*(realp value)*

*{realp value}*

Returns true if 'value' is a real type. In SETL is_real() checks if the value is a number with a radix expansion, in Lisp it just checks if it is any number (equivalent to is_number() in SETL without the support of complex numbers).


- - - - -

## integerp ##

*(integerp value)*

*{integerp value}*

Returns true if 'value' is an integer type.


- - - - -

## booleanp ##

*(booleanp value)*

*{booleanp value}*

Returns true if 'value' is a boolean type (t or nil, SETL false might still exist somewhere).


- - - - -

## stringp ##

*(stringp value)*

*{stringp value}*

Returns true if 'value is a string type.


- - - - -

## consp ##

*(consp value)*

*{consp value}*

Returns true if 'value' is a list type.


- - - - -

## listp ##

*(listp value)*

*{listp value}*

Returns true if 'value' is a list type.


- - - - -

## setlp ##

*(setlp value)*

*{setlp value}*

Returns true if 'value' is a set type.


- - - - -

## mapp ##

*(mapp value)*

Returns true if 'value' is a map. Note: Maps are an implicit type so where (mapp object) returns true - so will (setlp object) because a map is just a set of list pairs in the form {(a b) (c d) (e f)}


- - - - -

## symbol-name ##

*(symbol-name some-symbol)*

Returns a string of the symbol's name. (symbol-name 'this) ; returns "this"


- - - - -

## make-symbol ##

*(make-symbol some-string)*

Returns a symbol with the name some-string holds.


- - - - -

## defvar ##

*(defvar var-name value)*

Sets the symbol 'var-name' with 'value'.
