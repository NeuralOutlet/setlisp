

;; P01: Find the last box of a list
(defun get-last (lst)
	(if (> (length lst) 1)
		(get-last (cdr lst))
		lst))

@1= (list 4)
(get-last (list 1 2 3 4))


;; P02: Find the last but one box of a list
(defun get-last-two (lst)
	(if (> (length lst) 2)
		(get-last-two (cdr lst))
		lst))

@2= (list 3 4)
(get-last-two (list 1 2 3 4))


;; P03: Find the K-th element of a list
(defun kth (k lst)
	(if (> k 1)
		(kth (- k 1) (cdr lst))
		(car lst)))

@3= 'c
(kth 3 (list (quote a) (quote b) (quote c) (quote d)))


;; P04: Find the number of elements of a list
(defun cardinality (lst)
	(if (null lst)
		0
		(+ 1 (cardinality (cdr lst)))))

@4= 5
(cardinality (list 1 2 3 4 5))


;; P05: Reverse a list
(defun rev (lst)
	(rev2 lst (list)))

(defun rev2 (lst tmp)
	(if (null lst)
		tmp
		(rev2 (cdr lst) (cons (car lst) tmp))))

@5= (list 4 3 2 1)
(rev (list 1 2 3 4))


;; P06: Find out whether a list is a palindrome
(defun palinp (lst)
	(equal lst (rev lst)))

@6= t
(palinp (list 1 2 3 2 1))

@6= nil
(palinp (list 1 2 3 4 5))


;; P07: Flatten a nested list structure
(defun flatten (lst)
	(rev (get-list lst (list))))

(defun get-list (lst tmp)
	(if (null lst)
		tmp
		(if (consp (car lst))
			(get-list (cdr lst) (append (get-list (car lst) (list)) tmp))
			(get-list (cdr lst) (cons (car lst) tmp)))))

@7= (list 1 2 3 4 5)
(flatten (list 1 (list 2 (list 3 4) 5)))


;; P08: Eliminate consecutive duplicates of list elements
(defun remove-dup (lst)
	(rm-dup lst (list)))

(defun rm-dup (lst tmp)
	(if (null (car lst))
		(rev tmp)
		(if (equal (car lst) (car tmp))
			(rm-dup (cdr lst) tmp)
			(rm-dup (cdr lst) (cons (car lst) tmp)))))

@8= (list 1 2 3 4 1 2)
(remove-dup (list 1 1 1 2 3 3 3 4 4 1 1 1 1 1 1 1 1 1 2))


; P09: Pack consecutive duplicates of list elements into sublists
(defun pack (input)
        (pack-lst nil (list (car input)) (cdr input)))

(defun pack-lst (output packet input)
	(if (null input)
		(rev (cons packet output))
		(if (equal (car input) (car packet))
			(pack-lst output (cons (car input) packet) (cdr input))
			(pack-lst (cons packet output) (list (car input)) (cdr input)))))

@9= '((a a a a) (b) (c c) (a a) (d) (e e e e))
(pack '(a a a a b c c a a d e e e e))


; P10: Run-length encoding of a list
(defun run-encode (input)
	(count-packets nil (pack input)))

(defun count-packets (output input)
	(if (null input)
		(rev output)
		(count-packets (cons (list (length (car input)) (car (car input))) output) (cdr input))))

@10= '((4 a) (1 b) (2 c) (2 a) (1 d) (4 e))
(run-encode '(a a a a b c c a a d e e e e))

