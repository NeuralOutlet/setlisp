
(defvar *failed-tests* 0)

(defun equal-macro (left right)
	(format *error-output* "~%  Failed~%    The following expressions are not equal:~%")
	(format *error-output* "      ~a  evals to ~a~%" left (eval left))
	(format *error-output* "      ~a  evals to ~a~%" right (eval right))
	(setq *failed-tests* (+ 1 *failed-tests*)))

(defun true-macro (obj)
	(format *error-output* "  Failed~%    The following expression is not true:~%")
	(format *error-output* "      ~a  evals to ~a~%" obj (eval obj))
	(setq *failed-tests* (+ 1 *failed-tests*)))

(defun false-macro (obj)
	(format *error-output* "  Failed~%    The following expression is not false:~%")
	(format *error-output* "      ~a  evals to ~a~%" obj (eval obj))
	(setq *failed-tests* (+ 1 *failed-tests*)))

(defun test-read-macro (s c n)
	(let ((failcount *failed-tests*))
		(if (equal c #\=)
			(let ((left (read s))
			      (right (read s)))
				(if (not (equal (eval left) (eval right)))
					(equal-macro left right))))
		(if (equal c #\t)
			(let ((obj (read s)))
				(if (not (equal (eval obj) t))
					(true-macro obj))))
		(if (equal c #\f)
			(let ((obj (read s)))
				(if (equal (eval obj) t)
					(false-macro obj))))
		(if (= *failed-tests* failcount)
			(format t "~a)  Passed~%" n)
			(format t "~a)  Failed~%" n))))

(make-dispatch-macro-character #\@)
(set-dispatch-macro-character #\@ #\= #'test-read-macro)
(set-dispatch-macro-character #\@ #\t #'test-read-macro)
(set-dispatch-macro-character #\@ #\f #'test-read-macro)

(terpri)
(format t "===========~%")
(format t "Smoke Tests~%")
(format t "-----------~%")
@= (list 1 2 3) (list 1 2 3)
@f (list 1 2 3) (list 0 1 2)
@t (null (list))
(format t "===========~%")
(terpri)

(defun run-testfile (filename)
	(or
		#+sbcl (load filename :verbose t)
		#+ecl (load filename :verbose t)
		#+setlisp (load filename))
	(terpri))

(format t "===========~%")
(format t "Unit Tests~%")
(format t "-----------~%")
(loop for testgroup in
	(or
		#+setlisp (directory "tests/unit/*")
		#+sbcl (directory "tests/unit/*.lisp")
		#+ecl (directory "tests/unit/*.lisp"))
	do (run-testfile testgroup))
(format t "===========~%")
(terpri)

(format t "===========~%")
(format t "Integration Tests~%")
(format t "-----------~%")
(loop for test in
	(or
		#+setlisp (directory "tests/integration/*")
		#+sbcl (directory "tests/integration/*.lisp")
		#+ecl (directory "tests/integration/*.lisp"))
	do (run-testfile test))
(format t "===========~%")
(terpri)

(if (> *failed-tests* 0)
	(progn
		(format *error-output* "~%  ~a tests failed.~%" *failed-tests*)
		(or
			#+setlisp (exit 1)
			#+sbcl (sb-ext:quit)
			#+ecl (si:quit))))
