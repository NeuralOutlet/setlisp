

@0= nil '()
@1t (not nil)
@2f nil

@3t (numberp 0)
@4t (numberp 5)
@5f (numberp 'five)

@6t (realp 0)
@7t (realp 1)
@8t (realp 3.1415926)
@9f (realp "5.5")

@10t (integerp 0)
@11t (integerp 1)
@12f (integerp 1.618033)

@13t (stringp "String")
@14f (stringp 'symbol)
@15f (stringp 0)

@16t (listp (list 1 2 3))
@17t (listp '(1 2 3 4 5))
@18t (listp (cons 0 nil))
@19f (listp  "1 2 3 4 5")

@20=
(list 1 2 3)
(cons 1 (cons 2 (cons 3 nil)))

@21t (consp (list 1 2 3))
@22t (consp (cons 0 nil))


