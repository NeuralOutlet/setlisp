
;; defun with and without params
@0= (defun foo () "No params") 'foo
@1= (foo) "No params"

@2= (defun foo (bar) (* bar -1)) 'foo
@3= (foo -9) 9

@4= (defun blah (bar blah bloo) blah) 'blah
@5= (blah 1 2 3) 2

;; function/funcall
@6= (funcall (function abs) -3) 3
@7= (funcall (function foo) -3) 3
@8f
(equal
	(function abs)
	(function not))
@9=
(type-of (function foo))
(type-of (function car))

;; function macro
@10=
#'foo
(function foo)

@11=
#'abs
(function abs)
