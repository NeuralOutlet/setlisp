
;; can't test leave (quit/exit), eval-setl, or values here

;; char/code

@0= (char-code #\I) 73
@1= (code-char 100) #\d

;; eval

@2t (eval t)
@3f (eval (list))
@4f (eval '())
