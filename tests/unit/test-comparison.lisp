
;;;; Comparison Functions

@0t (null (not t))

@1t (not nil)
@2f (not t)

@3t (= 1 1)
@4= '() nil

@5t (> 2 1)

@6t (< 1 2)

@7t (>= 2 2)
@8t (>= 2 1)

@9t (<= 2 2)
@10t (<= 1 2)

@11t (oddp 3)
@12f (oddp 2)
