
;; Lists

@0=
(list 1 2 3 4)
'(1 2 3 4)

@1=
(list 1 (list 2 (list 3 4) 5 6) 7)
'(1 (2 (3 4) 5 6) 7)

@2= (car (list 1 2 3)) 1
@3= (cdr (list 1 2 3)) (list 2 3)
@4= (cons 1 nil) (list 1)
@5= (car nil) nil
@6= (car '()) nil
@7= (car (list)) nil

@8=
(subseq (list 1 2 3 4) 2 3)
(list 3)
@9=
(subseq (list 1 2 3 4) 1 2)
(list 2)
@10=
(subseq (list 1 2 3 4) 3 4)
(list 4)

@11=
(concatenate 'list (list 1 2 3 4) (list 5 6 7 8))
(list 1 2 3 4 5 6 7 8)

;; Strings

@12=
(concatenate 'string "this" " " "and" " " "this")
"this and this"

@13=
(format nil "this ~a this" "and")
"this and this"

@14= (char "this and this" 2) #\i
