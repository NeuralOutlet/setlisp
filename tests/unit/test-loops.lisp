
@0=
(loop for item in (list 1 2 3 4 5) collect item)
(list 1 2 3 4 5)

@1=
(loop for n from 0 below 5 collect n)
(list 0 1 2 3 4)

; Need to sort out strings and characters :/
;@2=
;(loop for ch across "this string" set-collect ch)
;(setl #\t #\h #\i #\s #\r #\n #\g)

@3=
(let ((i 0) (output 0))
	(loop until (equal i 10) do
		(progn
			(setq output (+ output 1))
			(setq i (+ i 1))))
	output)
10

@4=
(loop repeat 5 sum 5)
(+ 5 5 5 5 5)

@5=
(let ((i 0))
	(loop while (< i 10) collect
		(progn
			(setq i (+ i 1))
			(if (oddp i) i 0))))
(list 1 0 3 0 5 0 7 0 9 0)
