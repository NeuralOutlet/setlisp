# Running #

    # Running all files (*.lisp and *.scl)
    ./setlisp tests/testwrapper.lisp
    
    # Running just true Common lisp files
    # Tested against these three:
    clisp tests/testwrapper.lisp
    sbcl --script tests/testwrapper.lisp
    ecl -norc -shell tests/testwrapper.lisp

# Testcase Read Macros #

There are three read macros used for testcases:

    @=    ;checks equality of the two following objects
    @t    ;checks the following object is true
    @f    ;checks the following object is false

The number attribute of dispatch macros is used as the testcase id, an example of what it looks like:

    @1= 1 (+ 2 -1)
    @2t t
    @3t (consp (cons 0 nil))
    @4f (consp "String")
    
    @5=
    (list 1 2 3)
    '(1 2 3)
    
    @6=
    (concatenate 'string "Str" "ing")
    (concatenate 'string "S" "trin" "g")

The testwrapper.lisp file contains the layout for the logs, nothing special.
