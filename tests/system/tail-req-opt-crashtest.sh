
### Run the Collatz example without optimisation
###     Check for a crash
### Run it with optimisation
###     Check for no crash

# go to base dir
cd ../..

# run without opt
./setlisp examples/collatz.scl
if [ $? -eq 0 ]; then
	echo "Why is this not crashing?"
	exit 1
fi

#add the optimisation to the collatz code copy
echo "(setq *tail-req-opt* t)" > temp.lisp
cat examples/collatz.scl >> temp.lisp
./setlisp temp.lisp
retcode=$?
if [ $retcode -eq 0 ]; then
	echo "Success!"
else
	echo "Failed! :("
fi

rm temp.lisp
cd -
exit $retcode
